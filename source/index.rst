.. architecture4 documentation master file, created by
   sphinx-quickstart on Thu Jan  2 21:50:04 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

architecture4
=============

Contents:

.. toctree::
  :maxdepth: 1

  contents/01
  contents/02
  contents/03
  contents/04
  contents/05
  contents/06
  contents/07
  contents/08
  contents/09
  contents/10
  contents/11
  contents/12
  contents/13
  contents/14
  contents/15
  contents/16



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

